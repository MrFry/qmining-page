// eslint-disable-next-line no-undef
const useLocalhost = process && process.env.NODE_ENV === 'development'

const constants = {
  // FIXME: remove siteUrl, replace with Link
  siteUrl: useLocalhost ? 'http://localhost:8080/' : '/',
  apiUrl: useLocalhost ? 'http://localhost:8080/api/' : '/api/',
  mobileWindowWidth: 700,
  maxQuestionsToRender: 250,
  imageExts: ['gif', 'png', 'jpeg', 'jpg'],
  videoExts: ['mp4', 'mkv', 'webm'],
}

export default constants
