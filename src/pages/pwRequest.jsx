import React, { useState, useEffect } from 'react'
import Link from 'next/link'

import Header from '../components/header'

import styles from './pwRequest.module.css'
import constants from '../constants'

function fetchAvailablePWS() {
  return new Promise((resolve) => {
    fetch(`${constants.apiUrl}avaiblePWS`, {
      credentials: 'include',
    })
      .then((resp) => {
        return resp.json()
      })
      .then((res) => {
        resolve(res)
      })
  })
}

function requestPw() {
  return new Promise((resolve) => {
    fetch(constants.apiUrl + 'getpw', {
      method: 'POST',
      credentials: 'include',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({}),
    })
      .then((res) => {
        return res.json()
      })
      .then((res) => {
        resolve(res)
      })
  })
}

export default function PwRequest({ globalData, globalState, setGlobalState }) {
  const userId = globalData.userId || '...'
  const [result, setResult] = useState([])
  const [daysTillNext, setDaysTillNext] = useState('...')
  const [data, setData] = useState({
    userCreated: '...',
    availablePWS: '...',
    requestedPWS: '...',
    maxPWCount: '...',
    daysAfterUserGetsPWs: '...',
    dayDiff: '...',
    userCount: '...',
  })

  const {
    userCreated,
    availablePWS,
    requestedPWS,
    maxPWCount,
    daysAfterUserGetsPWs,
    dayDiff,
    userCount,
  } = data

  useEffect(() => {
    if (globalState.availablePWS) {
      setData(globalState.availablePWS)
    } else {
      fetchAvailablePWS().then((data) => {
        setData(data)
        setGlobalState({
          availablePWS: data,
        })
      })
    }
    const nextDate = 1
    const now = new Date()
    if (now.getDate() === nextDate) {
      setDaysTillNext(0)
      return
    }
    const now2 = new Date(now.getFullYear(), now.getMonth(), now.getDate())

    let next = new Date(
      now.getFullYear(),
      now.getMonth() + (now.getDate() < nextDate ? 0 : 1),
      nextDate
    )
    let daysTillNext = (next.getTime() - now2.getTime()) / 1000 / 60 / 60 / 24
    daysTillNext = (next.getTime() - now2.getTime()) / 1000 / 60 / 60 / 24

    setDaysTillNext(daysTillNext)
  }, [])

  return (
    <div>
      <Header title={'Jelszó generálás'} />
      <div className={'pageHeader'}>
        <h1>Jelszó generálás</h1>
      </div>
      <center>
        <div id="form">
          <div className={styles.text}>
            <p className={styles.descrip}>
              Minden felhasználó egyedi jelszót kap. Ne használjatok többen egy
              jelszót, mert egy idő után -biztonsági okokból- kidob a rendszer,
              ha ugyan az a felhasználó több helyen is belépve marad. A
              jelszavakról bővebben a <Link href="/faq?tab=pw">GYIK</Link>{' '}
              vonatkozó részében olvashatsz.
              <p />A jelszavak automatikusan és azonnal szinkronizálva vannak
              peerek között. Egy jelszó minden peer-hez jó
            </p>
          </div>
          <div className={styles.text}>
            Havonta minden felhasználó
            <span>{' ' + maxPWCount + ' db '}</span>
            jelszót generálhat. Új felhasználóknak
            <span>{' ' + daysAfterUserGetsPWs + ' '}</span>
            napot kell várniuk, hogy tudjanak jelszót generálni, de a
            <span>{' ' + daysAfterUserGetsPWs + '. '}</span>
            nap azonnal kapnak
            <span>{' ' + maxPWCount + ' db '}</span>
            lehetőséget.
            <p />
            Jelenleg
            <span>{' ' + availablePWS + ' db '}</span>
            jelszót generálhatsz még. Eddig összesen
            <span>{' ' + requestedPWS + ' '}</span>
            jelszót generáltál.{' '}
            {daysTillNext === 0 ? (
              <>
                <span>Ma éjfél</span> után
              </>
            ) : (
              <>
                <span>{daysTillNext} </span>nap múlva
              </>
            )}{' '}
            áll rendelkezésre ismét a maximum elérhető jelszógenerálási
            lehetőség
            <p />A jelenleg bejelentkezett felhasználó (
            <span>{' #' + userId + ' '}</span>)
            <span>{' ' + dayDiff + ' '}</span>
            napos,{' '}
            <span>
              {userCreated ? new Date(userCreated).toLocaleString() : '...'}
            </span>
            -kor lett létrehozva.
            <p />
            Az oldalnak jelenleg
            <span>{' ' + userCount + ' '}</span>
            felhasználója van.
          </div>
          <div className={'buttonContainer'}>
            <div
              onClick={() => {
                requestPw().then((res) => {
                  setData(res)
                  setGlobalState({
                    availablePWS: res,
                  })
                  if (res.success) {
                    setResult([...result, res.pw])
                  } else {
                    setResult([
                      ...result,
                      'Jelszó kérési lehetőségeid elfogytak, nézz vissza később',
                    ])
                  }
                })
              }}
            >
              Jelszó kérése
            </div>
          </div>

          {result ? (
            <div className={styles.pwContainer}>
              {result.map((res, i) => {
                return (
                  <div key={i} className={styles.pw}>
                    {i + 1}.: {res}
                  </div>
                )
              })}
            </div>
          ) : null}
        </div>
      </center>
    </div>
  )
}
