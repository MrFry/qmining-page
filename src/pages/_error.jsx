import React from 'react'

function Error({ statusCode }) {
  const render404 = () => {
    return (
      <center>
        <h1>404</h1>
        <iframe
          width="100%"
          height="465"
          src="https://www.youtube-nocookie.com/embed/GOzwOeONBhQ"
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      </center>
    )
  }

  if (statusCode === 404) {
    return render404()
  } else {
    return (
      <p>
        {statusCode
          ? `An error ${statusCode} occurred on server`
          : 'An error occurred on client'}
      </p>
    )
  }
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return { statusCode }
}

export default Error
