import React from 'react'

export default function Custom404() {
  return (
    <center>
      <h1>404</h1>
      <iframe
        width="660"
        height="465"
        src="https://www.youtube-nocookie.com/embed/GOzwOeONBhQ"
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      />
    </center>
  )
}
