import React, { useEffect, useState } from 'react'
import { useQuery } from 'react-query'

import Header from '../components/header'
import Link from 'next/link'

import constants from '../constants'
import ExternalLinkIcon from '../components/externalLinkIcon'

import styles from './script.module.css'

function fetchSupportedSites() {
  return new Promise((resolve) => {
    fetch(`${constants.apiUrl}supportedSites`, {
      credentials: 'include',
    })
      .then((resp) => {
        return resp.json()
      })
      .then((res) => {
        resolve(res)
      })
  })
}

export default function Script() {
  const { data: supportedSites } = useQuery('supportedSites', () =>
    fetchSupportedSites()
  )
  const [exampleHost, setExampleHostHost] = useState(
    'a szerver domainje ahova csatlakozni szeretnél (pl.: qmining.net "/" és "http" nélkül)'
  )

  useEffect(() => {
    if (typeof window !== 'undefined') {
      setExampleHostHost(window.location.host)
    }
  }, [])

  return (
    <div className={styles.content}>
      <Header title={'Script'} />
      <div className={'pageHeader'}>
        <h1>Script</h1>
      </div>
      <div className={'buttonContainer'}>
        <a
          href="https://www.tampermonkey.net/"
          target="_blank"
          rel="noreferrer"
        >
          Ajánlott userscript kezelő bővítmény
          <ExternalLinkIcon size={15} />
        </a>
        <a href="https://gitlab.com/MrFry/moodle-test-userscript/-/raw/master/stable.user.js">
          Script telepítése
          <ExternalLinkIcon size={15} />
        </a>
      </div>
      <div>
        <p>
          Ez a userscript Moodle/Elearnig/KMOOC tesztek megoldása során
          segítséget jelenít meg.
        </p>
        {supportedSites && (
          <>
            <b>Script jelenleg a következő oldalakon működik:</b>
            <ul>
              {supportedSites.map((ss) => {
                return <li key={ss}>{ss}</li>
              })}
              <li>
                <Link href="/faq?tab=addsite">
                  új oldal hozzáadásának menete itt
                </Link>
              </li>
            </ul>
          </>
        )}
        <b>Telepítés lépései:</b>
        <ol>
          <li>
            Tölts le egy userscript futtató kiegészítőt a böngésződhöz: pl. a{' '}
            <a
              href="https://www.tampermonkey.net/"
              target="_blank"
              rel="noreferrer"
            >
              Tampermonkey
            </a>
            -t
          </li>
          <li>
            <a
              href={`https://gitlab.com/MrFry/moodle-test-userscript/-/raw/master/stable.user.js`}
              target="_blank"
              rel="noreferrer"
            >
              Kattints ide hogy felrakd a scriptet
            </a>{' '}
          </li>
          <li>
            Ha megnyitod az egyik támogatott oldalon a scriptet, akkor
            használata előtt engedélyezned kell, hogy a szerverrel kommunikáljon
            a script.{' '}
            <b>
              Figyelem! Ezt minden új peer választás után engedélyezned kell!
              Előfordulhat, hogy először nem sikerül kapcsolódnia, ekkor
              frissítsd az oldalt.
            </b>
          </li>
          <li>
            Ezután a script egy apró menü ablakot jelenít meg a weboldal bal
            alsó részén
          </li>
          <li>
            Előfordulhat hogy mindenek előtt egy domaint fog kérni tőled, ide a
            következőt írd be: <b>{exampleHost}</b>
          </li>
          <li>
            Ha be vagy jelentkezve, akkor a teszt oldalakon a megoldásokat
            kellene látnod egy felugró ablakban felül. Ha nem, akkor{' '}
            <Link href="/faq">GYIK</Link>
            -be olvass bele, vagy{' '}
            <Link href="/contact">írj üzenetet mi nem működik</Link>
          </li>
        </ol>
        <b>Képek:</b>
        <ul>
          <li>
            Új host-hoz kapcsolódás engedélyezés oldal
            <br />
            <img
              style={{ width: '70%' }}
              src="img/scriptimg/script-6.jpg"
              alt="img"
            />
          </li>
          <li>
            Menü domain megadása előtt:
            <br />
            <img src="img/scriptimg/script-5.jpg" alt="img" />
          </li>
          <li>
            Menü bejelentkezés előtt / után:
            <br />
            <img src="img/scriptimg/script-2.jpg" alt="img" />
            <img src="img/scriptimg/script-1.jpg" alt="img" />
            <br />
          </li>
          <li>
            Teszt közben felugró ablak a kérdéssel és válasszal:
            <br />
            <img src="img/scriptimg/script-3.jpg" alt="img" />
          </li>
        </ul>
        <b>Amiket az ablakokkal lehet csinálni:</b>
        <ul>
          <li>
            Görgővel lehet az ablakok áttetszőségét állítani (menü / felugró
            ablakét külön)
          </li>
          <li>
            Az felső üzenet ablakot meg lehet fogni és mozgatni a szélénél (ahol
            a kurzor átvált)
          </li>
          <li>
            Jobb felső X gombbal be lehet zárni az ablakot, illetve ahol lehet
            mozgatni ott középső egér gombra be is zárul{' '}
          </li>
        </ul>
        <b>Egyéb fontos tudnivalók:</b>
        <ul>
          <li>
            <Link href="/allQuestions">
              Itt elérhető online az összes kérdés
            </Link>{' '}
            ha esetleg a script valamiért nem működne.
          </li>
          <li>
            <a
              href={`${constants.apiUrl}allqr.txt`}
              target="_blank"
              rel="noreferrer"
            >
              Itt megtalálható az összes kérdés letölthető TXT változatban
            </a>{' '}
            ha esetleg nem lenne elérhető a szerver, vagy bármi hiba történne
          </li>
        </ul>
      </div>
    </div>
  )
}
