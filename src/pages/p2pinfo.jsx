import React, { useState, useEffect } from 'react'

import Header from '../components/header'
import constants from '../constants'
import LoadingIndicator from '../components/LoadingIndicator'

import styles from './p2pinfo.module.css'

const infos = [
  {
    title: 'Név',
    key: 'name',
  },
  {
    title: 'Kontakt',
    key: 'contact',
  },
  {
    title: 'Utolsó szinkronizálás',
    key: 'lastSync',
    type: 'date',
  },
  {
    title: 'Felhasználók',
    key: 'userCount',
    type: 'number',
  },
  {
    title: 'Kérdés DB-k',
    key: 'questionDbCount',
    type: 'number',
  },
  {
    title: 'Tárgyak',
    key: 'subjectCount',
    type: 'number',
  },
  {
    title: 'Kérdések',
    key: 'questionCount',
    type: 'number',
  },
  {
    title: 'Script version',
    key: 'scriptVersion',
  },
  {
    title: 'Szerver build time',
    key: 'serverBuildTime',
    type: 'date',
  },
  {
    title: 'Weboldal build time',
    key: 'qminingPageBuildTime',
    type: 'date',
  },
  {
    title: 'Data editor build time',
    key: 'dataEditorBuildTime',
    type: 'date',
  },
  {
    title: 'Szerver utolsó commit date',
    key: 'serverLastCommitDate',
    type: 'date',
  },
  {
    title: 'Script utolsó commit date',
    key: 'scriptLastCommitDate',
    type: 'date',
  },
  {
    title: 'Weboldal utolsó commit date',
    key: 'qminingPageLastCommitDate',
    type: 'date',
  },
  {
    title: 'Data editor utolsó commit date',
    key: 'dataEditorLastCommitDate',
    type: 'date',
  },
  {
    title: 'Szerver revision',
    key: 'serverRevision',
  },
  {
    title: 'Script revision',
    key: 'scriptRevision',
  },
  {
    title: 'Weboldal revision',
    key: 'qminingPageRevision',
  },
  {
    title: 'Data editor revision',
    key: 'dataEditorRevision',
  },
]

function getDateString(dateNumber) {
  if (Number.isNaN(+dateNumber)) {
    return ' - '
  }
  return new Date(dateNumber).toLocaleString()
}

export default function P2PInfo({ globalState, setGlobalState }) {
  const [p2pInfo, setP2pinfo] = useState()
  const info = p2pInfo
    ? {
        ...p2pInfo,
        ...p2pInfo.selfInfo,
        ...p2pInfo.qdbInfo,
      }
    : {}

  useEffect(() => {
    if (globalState.p2pinfo) {
      setP2pinfo(globalState.p2pinfo)
    } else {
      fetch(constants.apiUrl + 'p2pinfo', {
        method: 'GET',
        credentials: 'include',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then((res) => {
          return res.json()
        })
        .then((res) => {
          setP2pinfo(res)
          setGlobalState({
            p2pinfo: res,
          })
        })
    }
  }, [])

  if (!p2pInfo) return <LoadingIndicator />

  return (
    <>
      <div className={'pageHeader'}>
        <h1>Peer to peer infó</h1>
      </div>
      <div className={styles.container}>
        <Header title={'P2P infó'} />
        <div style={{ textAlign: 'center' }}>
          A weboldal peer to peer (p2p) megoldást implementál, folyamatosan
          megosztja az új kérdéseket és felhasználókat a lent megadott
          regisztrált szerverekkel
          <p />A szervert akár te is hostolhatod, érdeklődj a lentebb megadott
          kontakton
        </div>
        <hr />
        <div className={styles.title}>Szerver P2P információja:</div>
        <br />
        {infos.map((x) => {
          const { title, key, type } = x

          let text = info[key]
          switch (type) {
            case 'date':
              text = getDateString(text)
              break
            case 'number':
              text = text ? text.toLocaleString() : '-'
              break
          }
          return (
            <div key={key} className={styles.infoRow}>
              <div>{title}</div>
              <div>{text}</div>
            </div>
          )
        })}
        <hr />
        <div className={styles.title}>Regisztrált peer-ek:</div>
        <div className={styles.peerHeader}>
          <div>Név</div>
          <div>Host</div>
          <div>Kérdések szinkronizálva</div>
          <div>Felhasználók szinkronizálva</div>
          <div>Fájlok szinkronizálva</div>
        </div>
        {p2pInfo.myPeers.map((peer, i) => {
          return (
            <div key={i} className={styles.peerContainer}>
              <div>{peer.name}</div>
              <div>
                <a
                  href={`https://${peer.host}:${peer.port}`}
                  target={'_blank'}
                  rel="noreferrer"
                >
                  {peer.host}:{peer.port}
                </a>
              </div>
              <div>{getDateString(peer.lastQuestionsSync)}</div>
              <div>{getDateString(peer.lastUsersSync)}</div>
              <div>{getDateString(peer.lastUserFilesSync)}</div>
            </div>
          )
        })}
        {p2pInfo.myPeers.length === 0 && (
          <div className={styles.peerContainer}>
            Ennél a szervernél jelenleg nincs peer regisztrálva
          </div>
        )}
      </div>
    </>
  )
}
