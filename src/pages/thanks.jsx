import React from 'react'
import styles from './thanks.module.css'
import constants from '../constants'
import Head from 'next/head'

export default function Thanks() {
  return (
    <div>
      <Head>
        <title>Qmining - Thanks for the donate!</title>
      </Head>
      <div className={styles.container}>
        <div className={styles.title}>Thanks for the donate!</div>
        <div>
          <img src={`${constants.siteUrl}img/thanks.gif`} />
        </div>
      </div>
    </div>
  )
}
