import React, { useState, useEffect } from 'react'
import Link from 'next/link'

import Header from '../components/header'

import constants from '../constants'
import styles from './faq.module.css'

function PasswordSection() {
  return (
    <>
      <div className={'manualBody'}>
        <ul>
          <li>Minden felhasználónak más jelszava van.</li>
          <li>
            A jelszavak automatikusan és azonnal szinkronizálva vannak peerek
            között. Egy jelszó minden peer-hez jó
          </li>
          <li>Elvileg elég csak egyszer beírnod, és többet nem kell, de </li>
          <li>
            Mentsd le biztos helyre a jelszót, hogy később is meglegyen!
            <i>
              Jelenleg nincs elfelejtett jelszó funkció, ha elfelejted, akkor az
              örökre eltűnik!
            </i>
          </li>
          <li>
            Ha van jelszavad akkor <i>bizonyos határok között</i> te is{' '}
            <a
              href={`${constants.siteUrl}pwRequest?man`}
              target="_blank"
              rel="noreferrer"
            >
              tudsz generálni
            </a>{' '}
            másoknak.
          </li>
          <li>
            Saját jelszavadat ne oszd meg, mivel egyszerre korlátozott számú
            helyen lehetsz belépve, máshol automatikusan ki leszel léptetve.
          </li>
          <li>
            Mivel senkinek sincs felhasználóneve, csak egy UserID (amit
            scriptben bal alul, weboldalon jobb felül találsz), így az egész
            teljesen anonim. Emiatt a jelszavakat nem lehet megváltoztatni, hogy
            a szükséges komplexitás megmaradjon.
          </li>
        </ul>
      </div>
    </>
  )
}

function FAQSection() {
  return (
    <div className={'manualBody'}>
      <ul>
        <li>
          <b>
            Lehet-e valahol szerkeszteni, törölni, illetve manuálisan hozzáadni
            a meglévő kérdésekhez?
          </b>
          Igen,{' '}
          <a
            href={`${constants.siteUrl}dataeditor`}
            target="_blank"
            rel="noreferrer"
          >
            ezen az oldalon
          </a>{' '}
          van lehetőség erre.
        </li>
        <li>
          <b>
            Mennyire anonymus a weboldal/script, és kiderülhet-e hogy használom?
          </b>
          A weboldal teljesen anonymus, mindenki csak egy számként van
          nyilvántartva (ami a user ID), random generált uuidv4 jelszavak vannak
          (így nicknevek sem kellenek), és még IP cím sincs sehol letárolva
          szerver oldalon.
          <br />
          Több infó a <Link href="/faq?tab=risk">kockázatok résznél</Link>.
        </li>
        <li>
          <b>Hogyan tudok a projektben segíteni?</b>
          Szinte mindig végtelen javítanivaló és tennivaló a projekttel
          kapcsolatban. Ha segíteni szeretnél, akkor vedd fel a kapcsolat az
          adminnal chat-en, és kérd meg hogy igazítson útba egy fejlesztőhöz.
        </li>
      </ul>
    </div>
  )
}

function RiskSection() {
  return (
    <>
      Itt {'"admin"'} alatt a szerver, weboldal és script egyedüli üzemeltetője
      értendő, az egyetlen személy, aki hozzáfér akármilyen szolgáltatással
      kapcsolatos adathoz, pl.: szerver statisztikák, szerver operációs
      rendszere, beküldött fájlok, chat üzenetek. A weboldalon a felhasználó
      azonosítója: <i>#1</i>
      <br />A weboldalt több ember is hostolhatja különböző domaineken, így
      különböző hostolt szervereken az admin más személyt jelent!
      <ul>
        <li>
          <b>Adminnak küldött fájlok kezelése</b>A beküldött fájlokban
          (adminnnak chat-en küldött, vagy kapcsolat oldalon küldött fájlok)
          szerepelhet személyes adat, pl.: név. Ezekhez a fájlokhoz és
          információkhoz csak az admin fér hozzá. Ezek az információk nincsenek
          sehol gyűjtve.
        </li>
        <li>
          <b>
            Ha esetleg {'"'}lebukik{'"'} a szerver, és tárolt személyes infók
          </b>
          Semmi személyes információ nincs eltárolva szerveren. Egyedüli
          információ amit egyáltalán ki tudnék deríteni az amúgy is az IP címed,
          de az nincs letárolva. Ha nagyon gonosz lennék, akkor a script el
          tudná küldeni a neved és talán még 1-2 adatot a szervernek, de ezt nem
          teszi.
          <p />
          Bizonyíték? A script, weboldal és szerver teljesen nyílt forráskódú,{' '}
          ezek repository-ja publikus, és akármikor felülvizsgálható. A script a
          te böngésződben fut, tampermonkey bővítmény menüjében megtekinthető
          milyen kód fut pontosan, és frissítéseknél megtekinthetőek a
          változások.
          <p />A projeckt git repókhoz vezető linkek megtalálhatók az
          oldalsávban
        </li>
        <li>
          <b>Bármikor észrevehetik hogy használod a scriptet</b> Vannak rá
          módszerek, hogy a weboldalon érzékeljék hogy fut-e userscript, de ez
          ellen védekezik a script. <p />
          A script shadow-root hoz teszi hozzá az összes megjelenített
          elementet, így ezeket lehetetlen detektálni. A moodle semmiféleképpen
          nem látja, hogy milyen más oldalak vannak megnyitva a böngésződben.
          Nem látja az XMLHttp requesteket se, amit a script végez (böngésző
          kiegészítőn keresztül). Egy Matomo nevű script látja hogy milyen
          oldalakra navigálsz a moodle-ről, de a script nem linkekkel irányít
          át, hanem javascript eseménnyel, amit nem tud nyomon követni.
          <p />
          Aztán ki tudja ténylegesen hogy lehet, ezért{' '}
          <i>csak saját felelősségre használd a scriptet!</i>
          <p />A kérdésekre a válaszokat{' '}
          <a
            href={`${constants.siteUrl}dataeditor`}
            target="_blank"
            rel="noreferrer"
          >
            manuálisan is be lehet küldeni
          </a>
          , és teszt kitöltés közben helyes válaszokat a weboldalon is meg lehet
          keresni. Ez viszont azért lehet veszélyes, mert a moodle figyelheti
          mikor veszíti el a fókuszt a weboldal.
        </li>
        <li>
          <b>Bármikor leállhat a szerver</b>
          És akkor nem bírod megnézni a válaszokat. Erre van az{' '}
          <a
            href={`${constants.siteUrl}allqr.txt?man`}
            target="_blank"
            rel="noreferrer"
          >
            összes kérdés TXT
          </a>
          . A szervert több példányban (instance) is lehet futtatni, és az
          üzemeltetői össze bírják kapcsolni egymással. Így ha az egyik kiesne,
          a script automatikusan megnézi a többi szervert, hogy él-e
        </li>
        <li>
          <b>Akármelyik válasz rossz lehet</b>
          Pl.: ha a script rosszul menti le, vagy rossz kérdésre ad választ. A
          script minding a moodle szerint helyesnek vélt választ menti le, így
          ritkán előfordulhat hogy logikailag nem helyes a válasz, de a tanárok
          azt jelölték be helyesnek.
        </li>
      </ul>
    </>
  )
}

function WebsiteSaveSection() {
  return (
    <>
      <p>
        Ha hibát találsz, kérlek jelents. Hogy a hibákat a saját gépemen
        reprodukálni tudjam, és könnyen ki bírjam javítani, sokszor jól jön, ha
        egy lementett weboldalt megkapok, amin a hiba történik. Így lehet
        menteni egy oldalt:
      </p>
      <ul>
        <li>
          Lementett oldalon jobb klikk, oldal mentése
          <br />
          <img
            src="img/faq/sitesave-1.jpg"
            alt="img"
            className={'manual_img'}
          />
        </li>
        <li>
          A lementett oldal egy fájlból és mappából áll.
          <br />
          <img
            src="img/faq/sitesave-2.jpg"
            alt="img"
            className={'manual_img'}
          />
        </li>
        <li>
          A fájlt nyisd meg szövegszerkesztővel (notepad), és keress rá a
          nevedre, majd (csak a neved, ne a sort) töröld ki, és mentsd el. Amit
          beküldesz, azt csak az admin látja.{' '}
          <Link href="/faq?tab=risk">Több infó itt</Link>{' '}
        </li>
        <li>
          Ezután <Link href="/contact">itt</Link> tudsz írni a weboldal
          adminjának, hogy útba igazítson egy olyan fejlesztő felé, aki ezek a
          fájlok alapján ki tudja javítani a hibát
        </li>
      </ul>
      <p>
        Előfordulhat hogy a fejlesztőknek nincs hozzáférése semmilyen egyetemi
        oldalhoz, így csak lementett weboldallal tudják hatékonyan tesztelni a
        scriptet. Ezért hatalmas segítség ha felveszed a kapcsolatot adminnal ha
        hibával találkozol.
      </p>
    </>
  )
}

function ScriptSection() {
  return (
    <div className={'manualBody'}>
      <ul>
        <li>
          <b>Hogy kell a scriptet telepíteni, és milyen oldalakon működik? </b>
          <Link href="/script">Ezen az oldalon van leírva</Link>{' '}
        </li>
        <li>
          <b>Hogyan működik a script?</b>
          <ul>
            <li>
              Az egész folyamat a teszt ellenőrzés oldalon kezdődik, a script
              onnan beolvassa, és szervernek beküldi a helyes válaszokat
            </li>
            <li>
              Ezután ha újból kitöltöd a tesztet, vagy ugyanazzal a kérdéssel
              találkozol másik tesztben (pl.: gyakorló teszt után zh/vizsga)
              akkor ott megjeleníti a választ
            </li>
            <li>
              Más felhasználóknak köszönhetően már valószínűleg lesznek
              megoldások a tesztjeidhez. <Link href="/allQuestions">Itt</Link>{' '}
              meg tudod tekinteni, hogy vannak-e válaszok a tárgyadhoz.
            </li>
            <li>
              Ha teszt ellenőrzés oldal nem elérhető, vagy nem egyértelműen
              kitalálható a helyes válasz, akkor a script nem tud mit csinálni,
              nem ment le semmit
            </li>
          </ul>
        </li>
        <li>
          <b>
            A teszt ellenőrző oldal nem elérhető. Ilyenkor lehet valamit tenni,
            hogy mégis el legyenek mentve a válaszok?
          </b>
          A{' '}
          <a
            href={`${constants.siteUrl}dataeditor`}
            target="_blank"
            rel="noreferrer"
          >
            dataeditor
          </a>{' '}
          oldal erre is jó, a <i>Kitöltetlen tesztek</i> részhez a script
          automatikusan feltölti a kérdéseket még megoldás közben. Ehhez nincs
          lementve helyes válasz, de ezt te utólag bejelölheted, vagy
          beküldheted.
        </li>
        <li>
          <b>
            Olyan helyeken fut le a script, ahol nem kellene, vagy ideiglenesen
            ki akarom kapcsolni, el akarom rejteni
          </b>
          Tampermonkey bővítmény ikon böngészőben -{'>'} click -{'>'} a scriptet
          kapcsold ki. Csak ne felejtsd el visszakapcsolni {';)'}
        </li>
        <li>
          <b>
            Túl nagy a kérdést és a választ megjelenítő ablak, nem tudok a
            válaszra kattintani
          </b>
          A felugró ablakot ha minden jól megy akkor a szélénél fogva tudod
          mozgatni, vagy egeret rajtatartva a görgővel tudod állítani az
          áttetszőségét, vagy be tudod zárni jobb felül X-el, vagy egér középső
          gombbal.
        </li>
        <li>
          <b>Peer váltás után nem tudok bejelentkezni a scripten keresztül!</b>
          Peer (vagy másnéven host) váltás után ha még nem kapcsolódtál a
          megadott host-hoz engedélyezned kell a scriptnek hogy csatlakozzon
          hozzá. Ez a felugró ablak nem minding azonnal jelenik meg, néha
          előfordulhat hogy frissíteni kell az oldalt. Ha ezután sem működik
          ellenőrizd hogy elérhető-e a host
        </li>
        <li>
          <b>Script felugró ablakon szereplő adatok és gombok értelmezése</b>
          <img
            className={'manual_img'}
            src="img/scriptimg/script-3.jpg"
            alt="img"
          />
          <ul>
            <li>
              Felső középső szöveg:
              <br />
              [Talált kérdés tárgya] - [Talált kérdés adatbázisának neve]
            </li>
            <li>
              Jobb oldalt nyilak:
              <br />
              Ha több kérdés van egy oldalon, akkor azzal lehet váltogatni
            </li>
            <li>
              Bal oldalt felső szám (5./1.)
              <br />
              Azt jelöli, hogy éppen hányadik kérdés válasza van megjelenítve.
              Itt a 5./1. azt jelenti, hogy a tesztben a 5. kérdés 1. talált
              válasza.
            </li>
            <li>
              Bal oldalt %
              <br />A szerver szerint hány százalékban egyezik a talált válasz
              azzal, amit tényleg keresni kell
            </li>
            <li>
              Középen
              <br />
              Első sorban kérdés, másodikban válasz (ha a sor hosszú, akkor
              megtörhet)
            </li>
          </ul>
          <img
            className={'manual_img'}
            src="img/scriptimg/script-4.jpg"
            alt="img"
          />
          <ul>
            <li>
              Alul nyilak:
              <br />
              Itt egy kérdésre több találat van, így közöttük neked kell
              eldönteni, hogy mi a helyes válasz. Ez akkor van, ha a kérdés
              szövege ugyan az, de más válasz is lehet. A nyilak segítségével
              válogathatsz a válaszok között.
            </li>
            <li>
              Bal oldalt felső szám (1./3.)
              <br />
              Itt az 1. azt jelenti hogy az oldalon az első kérdéshez van
              megjelenítve a válasz, a 3. pedig azt, hogy a sok talált válaszok
              közül a 3.
            </li>
            <li>
              A kérdés mellett lévő [0]
              <br />A teszt kérdésben itt épp egy kép van, és azt jelenti a
              szögletes zárójelben lévő szám. A tesztben lévő képnél is
              megjelenik ez a [0] azonosító, hogy könnyen párosítani lehessen.
              <br />
              Ez akkor hasznos, ha képekhez tartozik a válasz. Ekkor figyelni
              kell, mert a képek nem biztos hogy jó sorrendben vannak a tesztben
              a talált válaszhoz képest, de a []-ben lévő azonosítók segítenek.
            </li>
          </ul>
        </li>
      </ul>
    </div>
  )
}

function AddSite() {
  return (
    <>
      Jelenleg az, hogy a script melyik oldalakon fut bele van égetve a script
      forráskódjába. Ez itt látható:{' '}
      <a
        rel="noreferrer"
        target="_blank"
        href="https://gitlab.com/MrFry/moodle-test-userscript/-/blob/master/user.js"
      >
        stable.user.js#L52
      </a>
      {'. '}
      Ez azért van így, hogy biztos csak olyan oldalakon fusson a script amit
      támogat is.
      <p />
      Ehhez persze kézzel hozzá lehet adni ideiglenesen bármilyen oldalt, hogy
      kipróbáld ott működik-e. Akár működik, akár nem kérlek szólj, és frissítem
      a scriptet hogy azon a domainen is fusson és működjön alapból.
      <p />
      Egyetem moodle oldalának hozzáadása a scripthez kézzel:
      <ul>
        <li>
          <Link href="/script">Először telepítsd a scriptet rendesen</Link>{' '}
        </li>
        <li>
          Böngészőben nyisd meg a Tampermonkey bővítményt <br />
          <img className={'manual_img'} src={`img/faq/siteadd-1.png`} />
          <br />
          Itt kattints a Dashboard-ra.
        </li>
        <li>
          Keresd meg a scriptet, majd kattints rá:
          <br />
          <img className={'manual_img'} src={`img/faq/siteadd-2.png`} />
        </li>
        <li>
          A felugró szerkesztőben keresd meg a következő sorokat:
          <br />
          <img className={'manual_img'} src={`img/faq/siteadd-3.png`} />
        </li>
        <li>
          Itt az egyik <i>@match ...</i> sor után egy új sorban írd be az
          egyetemed moodle oldalának címét. Ha ez a cím pl.:{' '}
          <i>https://moodle.egyetem.hu/main/akármi</i>, akkor a következőre írd
          át: <i>https://moodle.egyetem.hu/*</i>. Fenti képen látsz néhány
          példát. <i>A {'"*"'} a végén fontos!</i>
        </li>
        <li>
          Ezután mentsd el (ctrl + s, vagy file {'->'} save). Ha minden jól
          ment, akkor elég frissíteni a moodle oldalát, és a script futni fog
          rajta. Ezek után azon az oldalon amit beírtál meg kell jelennie a
          Tampermonkey menüjében is:
          <br />
          <img className={'manual_img'} src={`img/faq/siteadd-4.png`} />
        </li>
        <li>
          Ha a tesztek közben mégsem megy, akkor ellenőrizd, hogy a beírt moodle
          cím egyezik-e a teszt közbenivel. Ha kell egy kis segítség, kérdésed
          van, nem működik valami, vagy éppen működik, de alapból nincs
          hozzáadva: <Link href="/contact">írj a kapcsolat oldalon!</Link>
        </li>
      </ul>
    </>
  )
}

function P2PSection() {
  return (
    <>
      A szerverben peer to peer (p2p) funkcionalitás is implementálva van, így
      ha több szerver fut egyszerre, akkor azok meg tudják osztani egymás között
      a fontosabb adatokat, így az összes szerveren ugyanazok az adatok érhetőek
      el.
      <p />
      Ha az egyik szerver kiesne, a script a megadott szerverek közül választ
      egy újat.
      <p />A következő adatok a szerverek között azonnal szinkronizálva vannak:
      <ul>
        <li>
          <i>Felhasználók</i>: egy jelszóval az összes oldalra be tudsz lépni
        </li>
        <li>
          <i>Kérdés adatbázisok</i>: Az összes tárgy és kérdés elérhető a
          szervereken
        </li>
        <li>
          <i>Fájlok</i>: A {'"ZH-k/Vizsgák, segédanyagok"'} alatt található
          fájlok
        </li>
      </ul>
      <i>Nem</i> szinkronizált adatok:
      <ul>
        <li>Chat</li>
        <li>Fórum</li>
        <li>Motd (motto of the day)</li>
      </ul>
      <h4>Peerek kezelése scriptnél:</h4>
      Ha nincs alapértelmezett peer, akkor ez az ablak fog megjelenni:
      <br />
      <img src="img/scriptimg/script-5.jpg" alt="img" />
      <br />
      Itt a szerver domain-jét kell megadni a következő példa formátumban:
      {" 'qmining.com'"}, {"'https://'"} és {"'/'"}-ek nélkül
      <p />
      <img src="img/scriptimg/script-1.jpg" alt="img" />
      <br />
      Ennél az ablaknál a legördülő menüben ki lehet választani egy másik peert,
      vagy újat lehet hozzáadni. Ha olyan peer-hez próbál a script kapcsolódni,
      amihez még előtte nem kapcsolódott, akkor engedélyezned kell a felugró
      ablakban a tampermonkey-nak, hogy csatlakozhasson a domain-hez.
      <br />
      <img
        style={{ width: '70%' }}
        src="img/scriptimg/script-6.jpg"
        alt="img"
      />
      <p />A szervereknek 1-1 adminja van, akik belelátnak minden szerveren
      tárolt adatba, a szervereken ők a <i>#1</i> számú felhasználók. Errő
      többet a <Link href="/faq?tab=risk">kockázatok résznél</Link> tudtok
      olvasni.
      <p />
      További információk a szerver és a peer-ek állapotáról{' '}
      <Link href="/p2pinfo">itt</Link> találhatóak
    </>
  )
}

const pages = {
  faq: { name: 'Vegyes általános kérdések', component: FAQSection },
  script: {
    name: 'Script specifikus kérdések',
    component: ScriptSection,
  },
  pw: { name: 'Infó jelszavakról', component: PasswordSection },
  risk: {
    name: 'Kockázatok, személyes infók kezelése',
    component: RiskSection,
  },
  websitedl: {
    name: 'Teszt közbeni hiba jelentése',
    component: WebsiteSaveSection,
  },
  addsite: {
    name: 'Futtatás (még) nem támogatott oldalon',
    component: AddSite,
  },
  p2p: {
    name: 'Peer to peer információk',
    component: P2PSection,
  },
}

export default function FAQ({ router }) {
  const [currPage, setCurrPage] = useState(pages.faq)

  const renderCurrPage = (page) => {
    if (page) {
      return <page.component />
    } else {
      return null
    }
  }

  useEffect(() => {
    router.replace(`${router.asPath.replace('.html', '')}`, undefined, {
      shallow: true,
    })
  }, [])

  useEffect(() => {
    if (router.query.tab) {
      setCurrPage(pages[router.query.tab])
    }
  }, [router.query.tab])

  return (
    <div>
      <Header title={'GYIK'} />
      <div className={'pageHeader'}>
        <h1>Gyakran Ismételt Kérdések</h1>
      </div>
      <div className={'buttonContainer'}>
        {Object.keys(pages).map((key) => {
          const page = pages[key]
          return (
            <div
              className={`${page === currPage ? 'activeButton' : ''}`}
              key={key}
              onClick={() => {
                setCurrPage(page)

                router.replace(
                  `${router.pathname}?tab=${encodeURIComponent(key)}`,
                  undefined,
                  { shallow: true }
                )
              }}
            >
              {page.name}
            </div>
          )
        })}
      </div>
      <hr />
      <div className={styles.content}>{renderCurrPage(currPage)}</div>
      <hr />
    </div>
  )
}
