import React, { useState, useEffect } from 'react'
import Link from 'next/link'

import Header from '../components/header'
import FeedbackArea from '../components/feedbackArea'
import constants from '../constants'
import LoadingIndicator from '../components/LoadingIndicator'

import styles from './contact.module.css'

export default function Contact({ globalState, setGlobalState }) {
  const [contacts, setContacts] = useState()

  useEffect(() => {
    if (globalState.contacts) {
      setContacts(globalState.contacts)
    } else {
      fetch(constants.apiUrl + 'contacts.json', {
        method: 'GET',
        credentials: 'include',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then((res) => {
          return res.json()
        })
        .then((res) => {
          setContacts(res)
          setGlobalState({
            contacts: res,
          })
        })
    }
  }, [])

  return (
    <div>
      <Header title={'Kapcsolat'} />
      <div className={'pageHeader'}>
        <h1>Kapcsolat</h1>
      </div>
      <div>
        <div className={'subtitle'}>Chat</div>
        <div className={styles.text}>
          <Link href="/chat?user=1">
            Weboldal chat-jén keresztül beszélgetés admin-nal
          </Link>
          <br />
          Valós idejű chat, képeket és fájlokat is lehet küldeni.
        </div>
      </div>
      <div>
        <div className={'subtitle'}>Üzenet küldése</div>
        <div className={styles.text}>
          Weboldalon keresztül üzenetküldés admin-nak (feedback). Válasz chat-en
          érkezik majd (💬 ikon jobb felül)
        </div>
      </div>
      <FeedbackArea from={'contact'} allowFile />
      <div className={styles.container}>
        {contacts ? (
          <>
            <div className={styles.text}>
              Figyelem! A szerver üzemeltetőjének kontaktját a
              <Link href="/p2pinfo">{'"P2P infó"'}</Link>
              részlegen találod!
            </div>
            <div className={styles.contactsContainer}>
              {Object.keys(contacts).map((key) => {
                const { description, value, href } = contacts[key]
                return (
                  <div key={key}>
                    <div>{description}</div>
                    {href ? (
                      <a target="blank" rel="noreferrer" href={href}>
                        {value}
                      </a>
                    ) : (
                      <div>{value}</div>
                    )}
                  </div>
                )
              })}
            </div>
          </>
        ) : (
          <LoadingIndicator />
        )}
      </div>
    </div>
  )
}
