import React from 'react'

import Forum from '../components/forum'

import styles from './index.module.css'

export default function UserForum({
  router,
  globalData,
  globalState,
  setGlobalState,
}) {
  const motd = globalData.motd

  return (
    <>
      {motd && (
        <div className={styles.topMsg}>
          <div className={styles.title}>MOTD</div>
          {motd ? (
            <div dangerouslySetInnerHTML={{ __html: motd }} />
          ) : (
            <div>...</div>
          )}
        </div>
      )}
      <Forum
        router={router}
        globalState={globalState}
        setGlobalState={setGlobalState}
        globalData={globalData}
        forumName={'frontpage'}
      />
    </>
  )
}
