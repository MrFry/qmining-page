import React from 'react'

import Questions from './Questions'

import constants from '../constants'

const countReducer = (acc, subj) => {
  return acc + subj.Questions.length
}

export default function QuestionSearchResult({ data, searchTerm }) {
  let subjs = []
  let results = -1

  if (searchTerm) {
    subjs = data.reduce((acc, subj) => {
      const resultQuestions = subj.Questions.reduce((qacc, question) => {
        const keys = ['Q', 'A', 'data']
        keys.some((key) => {
          if (typeof question[key] !== 'string') {
            return false
          }
          if (
            question[key] &&
            question[key].toLowerCase().includes(searchTerm.toLowerCase())
          ) {
            qacc.push(question)
            return true
          }
        })
        return qacc
      }, [])
      if (resultQuestions.length > 0) {
        acc.push({
          Name: subj.Name,
          Questions: resultQuestions,
        })
      }
      return acc
    }, [])
    results = subjs.reduce(countReducer, 0)
  } else {
    results = data.reduce(countReducer, 0)
  }

  const renderCount = () => {
    return (
      <div className="resultContainer">
        {results === 0 && (
          <div>
            {`${results} találat. Az általad keresett kifejezés nem található, 
          próbáld bővíteni a keresési feltételt!`}
          </div>
        )}
        {results > 0 && searchTerm ? <div>{`${results} találat.`}</div> : null}
      </div>
    )
  }

  if (results > constants.maxQuestionsToRender) {
    return (
      <div>
        {searchTerm ? (
          <div className="resultContainer">
            Szűkítsd a keresési feltételeket a találatok megjelenítéséhez!
          </div>
        ) : null}
        <hr />
      </div>
    )
  } else {
    return (
      <div>
        <div>{renderCount()}</div>
        <div>
          <Questions subjs={subjs} searchTerm={searchTerm} />
        </div>
      </div>
    )
  }
}
