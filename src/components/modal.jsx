import React, { useEffect } from 'react'

import styles from './modal.module.css'

export default function Modal(props) {
  const { closeClick } = props

  const keyHandler = (event) => {
    if (event.key === 'Escape' && closeClick) {
      closeClick()
    }
  }

  useEffect(() => {
    document.addEventListener('keydown', keyHandler)
    document.body.classList.add('modal-open')
    return () => {
      document.removeEventListener('keydown', keyHandler)
      document.body.classList.remove('modal-open')
    }
  }, [])

  return (
    <div
      style={{
        cursor: closeClick ? 'pointer' : 'auto',
      }}
      onClick={() => {
        if (closeClick) {
          closeClick()
        }
      }}
      className={styles.modal}
    >
      <div
        onClick={(event) => {
          event.stopPropagation()
        }}
        className={styles.modalContent}
      >
        {closeClick && (
          <div
            className={styles.close}
            onClick={() => {
              closeClick()
            }}
          >
            ❌
          </div>
        )}
        <div className={styles.children}>{props.children}</div>
      </div>
    </div>
  )
}
