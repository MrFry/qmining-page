import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import styles from './tooltip.module.css'

const defWidth = 300
const defHeight = 250

export default function Tooltip({ children, text, opened, width, height }) {
  const [ref, setRef] = useState(React.createRef()) // eslint-disable-line
  const rect = ref.current ? ref.current.getBoundingClientRect() : null
  const h = height || defHeight // eslint-disable-line
  const w = width || defWidth // eslint-disable-line

  return (
    <span className={styles.tooltip} ref={ref}>
      {text && text()}
      {opened &&
        ReactDOM.createPortal(
          <span
            style={{
              width: `${w}px`,
              maxWidth: `${w}px`,
              height: `${h}px`,
              maxHeight: `${h}px`,
              top: rect ? rect.top - h - 15 + window.scrollY : 0,
              left: rect ? rect.left + window.scrollX : 0,
            }}
            className={styles.tooltiptext}
          >
            {children}
          </span>,
          document.body
        )}
    </span>
  )
}
