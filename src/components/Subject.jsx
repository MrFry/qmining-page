import React, { PureComponent } from 'react'

import Question from './Question'

class Subject extends PureComponent {
  render() {
    const { subj } = this.props

    if (subj) {
      return (
        <div>
          {subj.Questions.map((question, i) => {
            return <Question key={i} question={question} />
          })}
        </div>
      )
    } else {
      return <div />
    }
  }
}

export default Subject
