import React from 'react'
import constants from '../constants'

export default function Sleep() {
  const hours = new Date().getHours()
  if (hours < 4 || hours > 23) {
    return (
      <div>
        <center>
          <img
            style={{
              margin: '10px',
              width: '300px',
              border: '2px solid white',
            }}
            src={constants.siteUrl + 'img/aludni.jpeg'}
            title="Ezt a képet azert látod, mert ilyenkor már igazán nem ezen az oldalon kellene járnod"
          />
        </center>
      </div>
    )
  } else {
    return null
  }
}
