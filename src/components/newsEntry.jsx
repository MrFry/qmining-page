import React from 'react'

import ReactButton from './reactButton'
import Comments from './comments'
import Link from 'next/link'

import constants from '../constants'

import styles from './newsEntry.module.css'

const Media = ({ url, onClick }) => {
  const ext = url.split('.').reverse()[0]
  if (constants.imageExts.includes(ext.toLowerCase())) {
    return (
      <img
        style={{ cursor: 'pointer' }}
        onClick={() => {
          if (onClick) onClick()
        }}
        src={url}
      />
    )
  } else if (constants.videoExts.includes(ext.toLowerCase())) {
    return (
      <video controls style={{ maxHeight: '700px' }}>
        <source src={url} type={`video/${ext === 'mkv' ? 'mp4' : ext}`} />
      </video>
    )
  } else {
    return <div>Invalid media: {ext} :/</div>
  }
}

const Content = ({ admin, content, mediaPath, onMediaClick }) => {
  return (
    <>
      {mediaPath && (
        <div className={styles.mediaContainer}>
          <Media
            onClick={onMediaClick}
            url={`${constants.apiUrl}forumFiles/${mediaPath}`}
          />
        </div>
      )}
      {admin ? (
        <div
          className={styles.newsBody}
          dangerouslySetInnerHTML={{ __html: content }}
        />
      ) : (
        <div className={styles.newsBody}>{content}</div>
      )}
    </>
  )
}

export default function NewsEntry({
  newsItem,
  uid,
  onCommentReact,
  onNewsReact,
  onComment,
  onDelete,
  onPostDelete,
  showUpDownVote,
  hideAdminIndicator,
  onTitleClick,
}) {
  const { reacts, title, content, user, comments, date, admin, mediaPath } =
    newsItem
  const dateObj = new Date(date)

  const isPostAdmin = !hideAdminIndicator && admin

  return (
    <div className={styles.newsRoot}>
      <div
        className={`${styles.newsContainer} ${
          isPostAdmin && styles.adminPost
        }  ${!isPostAdmin && styles.userPost}  ${
          uid === user && styles.ownPost
        } ${uid === user && isPostAdmin && styles.adminPost}`}
      >
        <div className={styles.newsHeader}>
          <div
            onClick={() => {
              if (onTitleClick) onTitleClick()
            }}
            style={{ cursor: onTitleClick ? 'pointer' : 'default' }}
            className={styles.newsTitle}
          >
            {title}
          </div>
          <div className={styles.userinfo}>
            <Link
              href={`/chat?user=${user}`}
              title={`Chat #${user}-el`}
              className={'userId'}
            >
              User #{user}
            </Link>
            <div className={styles.newsDate} title={dateObj.toLocaleString()}>
              @
              {dateObj.getDate() !== new Date().getDate()
                ? dateObj.toLocaleDateString()
                : dateObj.toLocaleTimeString()}
            </div>
          </div>
        </div>
        <Content
          onMediaClick={onTitleClick}
          admin={admin}
          content={content}
          mediaPath={mediaPath}
        />
      </div>
      <div className={'actions'}>
        <Comments
          uid={uid}
          onReact={(path, reaction, isDelete) => {
            onCommentReact({ path, reaction, isDelete })
          }}
          onComment={onComment}
          onDelete={onDelete}
          comments={comments}
        />
        {uid === user ? (
          <span
            onClick={() => {
              const res = window.confirm('Törlöd a bejegyzést?')
              if (!res) return
              onPostDelete()
            }}
          >
            Törlés
          </span>
        ) : null}
        <ReactButton
          showUpDownVote={showUpDownVote}
          existingReacts={reacts}
          uid={uid}
          onClick={(reaction, isDelete) => {
            onNewsReact({ reaction, isDelete })
          }}
        />
      </div>
    </div>
  )
}
