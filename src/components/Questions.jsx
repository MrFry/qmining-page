import React, { PureComponent } from 'react'

import Question from './Question'

import styles from './Questions.module.css'

class Questions extends PureComponent {
  render() {
    const { subjs, searchTerm } = this.props

    return (
      <div>
        <hr />
        {subjs.map((subj, i) => {
          return (
            <div className={styles.questionBg} key={i}>
              <div className={styles.subjName}>{subj.Name}</div>
              {subj.Questions.map((question, i) => {
                return (
                  <Question
                    key={i}
                    question={question}
                    searchTerm={searchTerm}
                  />
                )
              })}
            </div>
          )
        })}
      </div>
    )
  }
}

export default Questions
