import React from 'react'
import Head from 'next/head'
import { useQueryClient } from 'react-query'

export default function Header(props) {
  const { title } = props
  const queryClient = useQueryClient()
  const globalData = queryClient.getQueryData(['infos'])

  const unreadString =
    globalData && globalData.unreads.length > 0
      ? `(${globalData.unreads.length}) `
      : ''
  const titleString = title ? `${title} - ` : ''

  return (
    <Head>
      <title>{`${unreadString}${titleString}Qmining${
        typeof window !== 'undefined' ? ` | ${window.location.host}` : ''
      }`}</title>
    </Head>
  )
}
