import React from 'react'

import styles from './upDownVote.module.css'

export default function UpDownVote({
  onUp,
  onDown,
  onClear,
  upvotes,
  downvotes,
  userId,
  disabled,
}) {
  const upvoted = upvotes.includes(userId)
  const downvoted = downvotes.includes(userId)

  return (
    <div className={styles.container}>
      <div
        title={upvotes.join(', ')}
        className={styles.action}
        onClick={(e) => {
          e.stopPropagation()
          if (disabled) {
            return
          }
          if (!upvoted) {
            onUp()
          } else {
            onClear()
          }
        }}
      >
        <div>👍</div>
        <div className={`${upvoted && styles.voted}`}>{upvotes.length}</div>
      </div>
      <div
        title={downvotes.join(', ')}
        className={styles.action}
        onClick={(e) => {
          e.stopPropagation()
          if (disabled) {
            return
          }
          if (!downvoted) {
            onDown()
          } else {
            onClear()
          }
        }}
      >
        <div>👎</div>
        <div className={`${downvoted && styles.voted}`}>{downvotes.length}</div>
      </div>
    </div>
  )
}
