import React from 'react'
import styles from './button.module.css'

export default function Button(props) {
  return (
    <div>
      <center>
        <a href={props.href}>
          <div className={styles.button}>{props.text}</div>
        </a>
      </center>
    </div>
  )
}
