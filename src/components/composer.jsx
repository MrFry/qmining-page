import React, { useState } from 'react'

import Modal from './modal'

import styles from './composer.module.css'
import constants from '../constants'

export default function Composer({ onSubmit, allowFile, fileOnly }) {
  const [editorShowing, setEditorShowing] = useState(false)
  const [val, setVal] = useState('')
  const [title, setTitle] = useState('')
  const [file, setFile] = useState()

  return (
    <>
      <div className={'buttonContainer'}>
        <div
          onClick={() => {
            setEditorShowing(true)
          }}
          className={styles.new}
        >
          Új poszt ...
        </div>
      </div>
      {editorShowing && (
        <Modal
          closeClick={() => {
            setEditorShowing(false)
          }}
        >
          <div className={styles.container}>
            <input
              placeholder={'Téma...'}
              type={'text'}
              required
              value={title}
              onChange={(e) => {
                setTitle(e.target.value)
              }}
            />
            {!fileOnly && (
              <textarea
                placeholder={'Írj ide valamit...'}
                required
                value={val}
                onChange={(e) => {
                  setVal(e.target.value)
                }}
              />
            )}
            {(allowFile || fileOnly) && (
              <input
                className={styles.fileInput}
                type="file"
                name="file"
                accept={`${constants.imageExts
                  .map((x) => `.${x}`)
                  .join(',')},${constants.videoExts
                  .map((x) => `.${x}`)
                  .join(',')}`}
                onChange={(e) => {
                  const selectedFile = e.target.files[0]
                  setFile(selectedFile)
                  if (!title) {
                    setTitle(
                      selectedFile.name.split('.').slice(0, -1).join('.')
                    )
                  }
                }}
              />
            )}
            <div className={`actions ${styles.composerAction}`}>
              <span
                onClick={() => {
                  if (!title) {
                    alert('Üres a téma!')
                    return
                  }
                  if (!val && !fileOnly) {
                    alert('Üres a tartalom!')
                    return
                  }
                  if (fileOnly && !file) {
                    alert('Kérlek tölts fel egy fájlt!')
                    return
                  }

                  if (allowFile || fileOnly) {
                    const ext = file.name.split('.').reverse()[0]
                    if (
                      !constants.imageExts.includes(ext.toLowerCase()) &&
                      !constants.videoExts.includes(ext.toLowerCase())
                    ) {
                      alert(
                        `Kérlek helyes formátum fájlt tölts fel! (${constants.imageExts.join(
                          ', '
                        )}, ${constants.videoExts.join(', ')})`
                      )
                      return
                    }
                  }

                  onSubmit(title, val, file)
                  setTitle('')
                  setVal('')
                  setFile(undefined)
                  setEditorShowing(false)
                }}
              >
                Posztolás
              </span>
            </div>
          </div>
        </Modal>
      )}
    </>
  )
}
