import React from 'react'

import styles from './LoadingIndicator.module.css'

export default function LoadingIndicator() {
  return (
    <div className={styles.loadContainer}>
      <div className={styles.load} />
    </div>
  )
}
