import React, { useState } from 'react'
import styles from './Selector.module.css'

export default function Selector(props) {
  const { activeItem, data, onSelect, getLength, getName } = props
  const [searchTerm, setSearchTerm] = useState('')

  const dataToDisplay = data.filter((item) => {
    return getName(item).toLowerCase().includes(searchTerm.toLowerCase())
  })

  return (
    <div className={styles.container}>
      <input
        type={'text'}
        onChange={(e) => setSearchTerm(e.target.value)}
        placeholder={'Kezdj el írni a kereséshez ...'}
      />
      <div className="selector">
        {dataToDisplay.length === 0 && (
          <div className={styles.noResult}>
            <span className={styles.itemName}>{'Nincs találat'}</span>
          </div>
        )}
        {dataToDisplay.map((item, i) => {
          return (
            <div
              className={
                activeItem && getName(activeItem) === getName(item)
                  ? 'selectorItem activeSelectorItem'
                  : 'selectorItem'
              }
              key={i}
              onClick={() => onSelect(item)}
            >
              <span className={styles.itemName}>{getName(item)}</span>
              {getLength && (
                <span className={styles.questionCount}>
                  [ {getLength(item)} ]
                </span>
              )}
            </div>
          )
        })}
      </div>
    </div>
  )
}
