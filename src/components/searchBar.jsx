import React from 'react'

import styles from './searchBar.module.css'

export default function SearchBar({ searchTerm, onChange }) {
  return (
    <center>
      <div className={styles.searchContainer}>
        <input
          placeholder="Kezdj el írni a kereséshez..."
          type="text"
          value={searchTerm}
          onChange={(event) => {
            onChange(event.target.value)
          }}
        />
        <button
          onClick={() => {
            onChange('')
          }}
          className={styles.clearButton}
        >
          ❌
        </button>
      </div>
    </center>
  )
}
