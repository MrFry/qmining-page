import React, { useState, useEffect, useRef } from 'react'

import Tooltip from './tooltip'

import styles from './reactButton.module.css'
import reactions from '../data/reactions.json'

const upDownVoteKeys = { 'thumbs up': [-1], 'thumbs down': [-1] }

function useOutsideAlerter(ref, action) {
  useEffect(() => {
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        action()
      }
    }

    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [ref])
}

function mergeEmptyUpDownVoteObjWithExisting(existingReacts) {
  if (!existingReacts) return upDownVoteKeys

  return Object.keys(existingReacts).reduce((acc, reactKey) => {
    return { ...acc, [reactKey]: existingReacts[reactKey] }
  }, upDownVoteKeys)
}

function ExistingReacts({ existingReacts, onClick, uid, showUpDownVote }) {
  const mergedReactions = showUpDownVote
    ? mergeEmptyUpDownVoteObjWithExisting(existingReacts)
    : existingReacts

  return (
    <>
      {mergedReactions &&
        Object.keys(mergedReactions).map((key) => {
          const currReact = mergedReactions[key]
          const react = reactions[key]
          if (!react) {
            return null
          }
          return (
            <span
              title={`'${key}': ${currReact.join(', ')}`}
              className={`${currReact.includes(uid) ? styles.reacted : ''}`}
              key={key}
              onClick={(e) => {
                e.stopPropagation()
                onClick(key, currReact.includes(uid))
              }}
            >
              {react.emoji} {currReact[0] === -1 ? 0 : currReact.length}
            </span>
          )
        })}
    </>
  )
}

function RenderEmojis({ onClick }) {
  const [search, setSearch] = useState('')

  return (
    <>
      <input
        autoFocus
        type="text"
        placeholder="Keresés..."
        onChange={(event) => {
          setSearch(event.target.value)
        }}
      />
      {Object.keys(reactions).map((key) => {
        const reaction = reactions[key]
        if (!key.includes(search.toLowerCase())) {
          return null
        }
        return (
          <div
            title={key}
            key={key}
            onClick={() => {
              onClick(key)
            }}
          >
            {reaction.emoji}
          </div>
        )
      })}
    </>
  )
}

export default function ReactButton({
  onClick,
  existingReacts,
  uid,
  showUpDownVote,
}) {
  const [opened, setOpened] = useState(false)
  const wrapperRef = useRef(null)
  useOutsideAlerter(wrapperRef, () => {
    setOpened(false)
  })

  return (
    <>
      <Tooltip
        width={300}
        height={250}
        text={() => {
          return (
            <span
              onClick={() => {
                setOpened(true)
              }}
            >
              Reakció
            </span>
          )
        }}
        opened={opened}
      >
        <div ref={wrapperRef} className={styles.reactionContainer}>
          <RenderEmojis
            onClick={(e) => {
              // setOpened(false)
              onClick(e)
            }}
          />
        </div>
      </Tooltip>
      <ExistingReacts
        showUpDownVote={showUpDownVote}
        uid={uid}
        onClick={(key, isDelete) => {
          onClick(key, isDelete)
          setOpened(false)
        }}
        existingReacts={existingReacts}
      />
    </>
  )
}
