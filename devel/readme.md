# Alapok

A weboldal Next.js-ben íródott, ami egy React.js keretrendszer

Next.js tutorial: https://nextjs.org/learn/basics/create-nextjs-app

React.js tutorial: https://reactjs.org/tutorial/tutorial.html

Bár React.js-re épül a keretrendszer, a Next.js tutorial sokkal könnyebb. De React.js alap tudás
szükséges.

Weboldal node-ban fut, amit innen lehet telepíteni:

https://nodejs.org/en/download/

Az innentől írt `parancsokat` a projekt mappájában nyitott parancssorban lehet futtatni (shift-klikk a
fájl böngészőben -> "parancssor nyitása itt", vagy VScode terminál)

Ajánlott IDE: VS code eslint kiegészítővel

# Névtelenség

Nagyon ajánlott, hogy névtelen maradj, ezért figyelj, hogy a git commitoknál ne az
igazi/felismerhető neved/nickneved legyen, és főleg ne az igazi e-mail címed!

# Beüzemelés

1. Szükséges npm csomagok telepítése:

  `npm install`

2. Devel szerver indítása

  `npm run dev`

3. Böngészőben megnyitás

  Elvileg a http://localhost:3000/ címen elérhető.

4. Fejlesztés !

  Innentől ha módosítod a projekt (majdnem) akármelyik fájlját és elmented, akkor a böngészőben
  láthatod a módosítás eredményét

# Mappa / fájl szerkezet

  * `src`

    Itt van minden forrás fájl

  * `devel`

    Fejlesztési információk és segítség

  * `out`

    Statikus export eredménye (több infó lentebb)

  * `public`

    Public mappa, mivel statikusan exportált fájlokat használ az éles szerver, ezért ennek nincs
    jelentősége

  * `make.sh`

    A projekt előkészítése éles verzióban lévő használathoz

# Statikusan exportált?

Bővebben: https://nextjs.org/docs/advanced-features/static-html-export

A weboldalt lehet statikus .html, .js és .css fájlokba exportálni, így elég egy fájl szervert indítani,
ami ezeket a fájlokat osztja meg. Az eredeti weboldal így működik élesben. Ebben az üzemmódban a
weboldal tökéletesen működik, néhány apró funkció nem működik csak, ami nincs is használva ebben a
projektben.

`npm run export`

A generált fájlok az 'out' mappában lesznek.

# Egyéb

 * Projekt eslint-el van formázva, és "lintelve", ami nincs hozzáadva a projekt moduljaihoz
 (globálisan rakd fel külön)
