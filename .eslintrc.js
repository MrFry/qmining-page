module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  parser: '@babel/eslint-parser',
  parserOptions: {
    sourceType: 'module',
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  plugins: ['react'],
  extends: [
    'plugin:react/recommended',
    'plugin:react/jsx-runtime',
    'plugin:@next/next/recommended',
  ],
  rules: {
    'react/prop-types': 0,
    'no-undef': ['error'],
    eqeqeq: ['warn', 'smart'],
    'react/jsx-uses-vars': 'error',
    'react/jsx-uses-react': 'error',
    '@next/next/no-img-element': 'off',
    'no-unused-vars': 'warn',
    'no-prototype-builtins': 'off',
    'id-length': [
      'warn',
      { exceptions: ['x', 'i', 'j', 't', 'Q', 'A', 'C', 'q', 'a', 'b', 'e'] },
    ],
  },
  root: true,
}
